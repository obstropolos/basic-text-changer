chrome.runtime.onMessage.addListener(gotMessage);
function gotMessage(message, sender, sendResponse) {
    console.log(message.txt);
    if (message.txt === "change text") {
        // with help from http://is.gd/mwZp7E
        walk(document.body);
        function walk(node) {
            var child, next;
            switch ( node.nodeType )  {
                case 1:  // Element
                case 9:  // Document
                case 11: // Document fragment
                    child = node.firstChild;
                    while ( child ) {
                        next = child.nextSibling;
                        walk(child);
                        child = next;
                    };
                    break;
                case 3: // Text node
                    handleText(node);
                    break;
            };
        };
        //Set up your changing text here
        function handleText(textNode) {
            var v = textNode.nodeValue;
            //Replacement happens from "example" to "changed" in the following:
            v = v.replace(/\bexample\b/g, "changed");
            textNode.nodeValue = v;
        };
    };
};

